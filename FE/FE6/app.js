const EventHandling = {
    data() {
        return {
            duration: "",
            title: "",
            mp3name: "",
            sideopen: false,
            create: "",
            playlistnames: [
                {
                    playlist: "Playlist1"
                },
                {
                    playlist: "Playlist2"
                },
                {
                    playlist: "Playlist3"
                }
            ],
            mp3file: [],
            song: [],
        }
    },
    methods: {

        open() {
            this.sideopen = true
        },
        close() {
            this.sideopen = false
        },
        modalC() {
            this.playlistnames.push({
                playlist: this.create
            })
            this.create = ""
            document.querySelector('#cancel').click();
        },
        dragover(event) {
            event.preventDefault()

        },
        drop(event) {
            event.preventDefault()
            this.mp3file = event.dataTransfer.files
            this.mp3name = this.mp3file[0].name
          
        },
        dragenter(event) {
            event.preventDefault()
        },
        dragleave(event) {
            event.preventDefault()
        },
        audios() {
            
            var th = this;
            var audio = document.getElementById('audio');
            var objectUrl = URL.createObjectURL(this.mp3file[0]);
            audio.src = objectUrl;
           
            audio.addEventListener("canplaythrough", function (e) {

                 
                var duration = th.mp3duration(audio.duration);
                th.title = th.mp3file[0].name
                
                th.duration = duration;
                th.song.push({
                    title: th.title,
                    artist: "none",
                    album: "none",
                    duration: th.duration
                
                })
              
            });
           
           
        },
        mp3duration(duration) {
            var h = Math.floor(duration / 3600);
            var m = Math.floor(duration % 3600 / 60);
            var s = Math.floor(duration % 3600 % 60);
            var hms = h + m + ":" + s;
            return hms; 
            
        },
    }
}

Vue.createApp(EventHandling).mount('#sides')
