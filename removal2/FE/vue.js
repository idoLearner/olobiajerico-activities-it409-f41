const app = Vue.createApp({
    data() {
        return {
            fname: "",
            lname: "",
            mname: "",
            email: "",
            password: "",
            confirm_password: "",
            registertab: false,
            userVerified: null,
            message: "",
            error: ""
        }
    },
    methods: {
        showlogin() {
            this.clear()
            this.registertab = false
            this.message = ""
            this.error = ""
        },
        showregister() {
            this.clear()
            this.message = ""
            this.error = ""
            this.registertab = true
        },
        login() {
            var formData = new FormData();
            formData.append('email', this.email);
            formData.append('password', this.password);
            instance.post('/login',
                formData
            ).then(({
                data
            }) => {
                this.userVerified = true
                localStorage.setItem("access_token", data.access_token);
                this.clear()
                this.name = data.user.fname + " " + data.user.mname + " " + data.user.lname
                window.location.href = "loggedin.html"
            }).catch((err) => {
                    this.error = "Incorrect username or password."
            });
        },
        register() {
            var email_input = document.querySelector("#email");
            var password_input = document.querySelector("#password");
            var confirm_password_input = document.querySelector("#confirm_password");
            var emailRegex = /^[^\s@]+@[^\s@]+$/;
            var err = false;
            if (!this.validate("fname"))
                err = true;
            if (!this.validate("mname"))
                err = true;
            if (!this.validate("lname"))
                err = true;

            if (!emailRegex.test(this.email)) {
                email_input.classList.add("is-invalid")
                err = true;
            }
            else{
                email_input.classList.remove("is-invalid")
            }
            if (this.password !== this.confirm_password || this.password == "") {
                password_input.classList.add("is-invalid")
                confirm_password_input.classList.add("is-invalid")
                err = true;
            }
            else{
                password_input.classList.remove("is-invalid")
                confirm_password_input.classList.remove("is-invalid")
            }
            if (!err) {
                this.error = ""
                this.message = ""
                var formData = new FormData();
                formData.append('fname', this.fname);
                formData.append('lname', this.lname);
                formData.append('mname', this.mname);
                formData.append('email', this.email);
                formData.append('password', this.password);
                formData.append('confirm_password', this.confirm_password);
                instance.post('/register',
                    formData
                ).then(({
                    data
                }) => {
                    this.message = "Succcessfuly Registered!"
                    this.clear()
                }).catch((err) => {
                    this.error = "Failed!"
                });
            }
        },
        validate(input) {
            var input = document.querySelector("#" + input);
            if (input.value == "") {
                input.classList.add('is-invalid');
                return false;
            } else {
                input.classList.remove('is-invalid');
                return true;
            }
        },
        logout() {
            instance.post('/logout').then(({
                data
            }) => {
                this.userVerified = false
                localStorage.removeItem("access_token")
                window.location.href = "index.html"
            }).catch((err) => {
                if (err.response.status == "422")
                    this.error = "Incorrect username or password."
            });
        },
        refresh() {
            instance.post('/refresh').then(({
                data
            }) => {
                console.log(data.access_token)
                localStorage.setItem("access_token", data.access_token);
                setHeaders()
                this.verify()
            }).catch((err) => {
                this.userVerified = false
            });
        },
        verify() {
            console.log(localStorage.getItem("access_token"))
            instance.get('/user-profile').then(({
                data
            }) => {
                this.name = data.fname + " " + data.mname + " " + data.lname
                this.userVerified = true
                userVerified()
            }).catch((err) => {});
        },
        clear() {
            this.fname = ""
            this.lname = ""
            this.mname = ""
            this.email = ""
            this.password = ""
            this.confirm_password = ""
        }
    },
    beforeMount() {
        this.refresh()
    }
});

app.mount("#vue")
