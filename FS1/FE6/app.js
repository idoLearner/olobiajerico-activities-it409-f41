const EventHandling = {
    data() {
        return {
            duration: "",
            title: "",
            mp3name: "",
            sideopen: false,
            create: "",
            header: "",
            playlistnames: [],
            p_index: null,
            editpl: "",
            tab: "",
            mp3file: [],
            song: [],
            availablesong: []
        }
    },
    methods: {

        open() {
            this.sideopen = true
        },
        close() {
            this.sideopen = false
        },
        modalC() {
            var dis = this;
            var formData = new FormData();
            formData.append('name', this.create);
            axios.post('http://127.0.0.1:8000/addplaylist',
                formData
            ).then(({
                data
            }) => {
                dis.playlistnames.push({
                    id: data,
                    playlist: this.create
                })
                dis.create = ""
            }).catch((err) => {});
            document.querySelector('#cancel').click();
        },
        dragover(event) {
            event.preventDefault()

        },
        drop(event) {
            event.preventDefault()
            this.mp3file = event.dataTransfer.files
            this.mp3name = this.mp3file[0].name

        },
        dragenter(event) {
            event.preventDefault()
        },
        dragleave(event) {
            event.preventDefault()
        },
        audios() {
            var title = "none";
            var artist = "none";
            var album = "none";
            var duration = "";

            var dis = this;
            var fi = this.mp3file;

            var jsmediatags = window.jsmediatags;
            jsmediatags.read(fi[0], {
                onSuccess: function (tag) {

                    var ta = tag.tags
                    if (ta.title) {
                        title = ta.title
                    } else {
                        title = fi[0].name
                    }
                    if (ta.artist) {
                        artist = ta.artist
                    }
                    if (ta.album) {
                        album = ta.album
                    }
                    var src = URL.createObjectURL(fi[0]);
                    var audio = document.querySelector("#audio");
                    audio.src = src;
                    audio.onloadedmetadata = function () {

                        var formData = new FormData();
                        formData.append('title', title);
                        formData.append('artist', artist);
                        formData.append('length', audio.duration);
                        formData.append('song', fi[0]);
                        axios.post('http://127.0.0.1:8000/addsong',
                            formData,
                            {
                                headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                            }
                        ).then(({
                            data
                        }) => {
                            duration = dis.mp3duration(audio.duration);
                            dis.song.push({
                                id: data,
                                title: title,
                                artist: artist,
                                album: album,
                                duration: duration
                            })
                        }).catch((err) => {});


                        document.querySelector(".cancel").click()

                        dis.file = []
                    };
                },
                onError: function (error) {

                    console.log(error);
                }
            });

        },
        mp3duration(duration) {
            var h = Math.floor(duration / 3600);
            var m = Math.floor(duration % 3600 / 60);
            var s = Math.floor(duration % 3600 % 60);
            var hms = h + m + ":" + s;
            return hms;

        },
        saveplaylist(i){
            this.p_index = i
            this.editpl = this.playlistnames[i].playlist
        },
        showplaylistsong(i) {
            this.p_index = i
            this.header = this.playlistnames[i].playlist
            var dis = this
            var id = this.playlistnames[i].id
            this.tab = id
            axios.get('http://127.0.0.1:8000/displaysongtoplaylist/'+id).then(({
                data
            }) => {
                this.song = []
                for (i = 0; i < data.length; i++) {
                    dis.song.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: "none",
                        duration: dis.mp3duration(data[i].length),
                        p_id: data[i].p_id
                    })
                }
            }).catch((err) => {});
        },
        addsong(i){
            var dis = this;
            var formData = new FormData();
            var song_id = this.availablesong[i].id
            var playlist_id = this.playlistnames[this.p_index].id
            formData.append('song_id', song_id);
            formData.append('playlist_id', playlist_id);
            axios.post('http://127.0.0.1:8000/addsongtoplaylist',
                formData
            ).then(({
                data
            }) => {
                if(dis.tab == playlist_id){
                    console.log(data)
                    dis.song.push({
                        id: dis.availablesong[i].id,
                        title: dis.availablesong[i].title,
                        artist: dis.availablesong[i].artist,
                        album: 'none',
                        duration: dis.availablesong[i].duration,
                        p_id: data
                    })
                }
                dis.availablesong.splice(i, 1)
            }).catch((err) => {});
        },
        showavailablesong(){
            var dis = this
            var id = this.playlistnames[this.p_index].id
            axios.get('http://127.0.0.1:8000/displaysongnotinplaylist/'+id).then(({
                data
            }) => {
                this.availablesong = []
                for (i = 0; i < data.length; i++) {
                    dis.availablesong.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: "none",
                        duration: dis.mp3duration(data[i].length)
                    })
                }
            }).catch((err) => {});
        },
        editplaylist(){
            var id = this.playlistnames[this.p_index].id
            var name = this.editpl
            var dis = this;
            var formData = new FormData();
            formData.append('id', id);
            formData.append('name', name);
            axios.post('http://127.0.0.1:8000/editplaylist',
                formData
            ).then(({
                data
            }) => {
                dis.playlistnames[dis.p_index].playlist = dis.editpl
                dis.editpl = ""
            }).catch((err) => {});
        },
        dletplylst(i) {
            var dis = this;
            var formData = new FormData();
            formData.append('id', this.playlistnames[i].id);
            axios.post('http://127.0.0.1:8000/deleteplaylist',
                formData
            ).then(({
                data
            }) => {
                dis.playlistnames.splice(i, 1)
                dis.displaysong()
            }).catch((err) => {});
        },
        deletesong(i){
            if(this.p_index == null){
                this.removesong(i)
            }
            else{
                this.removeplaylistsong(i)
                console.log('true')
            }
        },
        removesong(i) {
            var dis = this;
            var formData = new FormData();
            formData.append('id', this.song[i].id);
            axios.post('http://127.0.0.1:8000/deletesong',
                formData
            ).then(({
                data
            }) => {
                dis.song.splice(i, 1)
            }).catch((err) => {});
        },
        removeplaylistsong(i){
            var dis = this;
            var formData = new FormData();
            formData.append('id', this.song[i].p_id);
            console.log(this.song[i])
            axios.post('http://127.0.0.1:8000/deletesongfromplaylist',
                formData
            ).then(({
                data
            }) => {
                
                dis.song.splice(i, 1)
            }).catch((err) => {});
        },
        displaysong() {
            this.tab = ""
            this.p_index = null
            this.header = "All Songs"
            var dis = this
            axios.get('http://127.0.0.1:8000/displaying').then(({
                data
            }) => {
                this.song = []
                duration = dis.mp3duration(audio.duration);
                for (i = 0; i < data.length; i++) {
                    dis.song.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: "none",
                        duration: dis.mp3duration(data[i].length)
                    })
                }
            }).catch((err) => {});
        },
        displaylst() {
            var dis = this
            axios.get('http://127.0.0.1:8000/displayplaylist').then(({
                data
            }) => {
                console.log(data)
                for (i = 0; i < data.length; i++) {
                    dis.playlistnames.push({
                        id: data[i].id,
                        playlist: data[i].name,
                    })
                }
            }).catch((err) => {});


        }
    },
    beforeMount() {
        this.displaysong()
        this.displaylst()
    }
}

Vue.createApp(EventHandling).mount('#sides')