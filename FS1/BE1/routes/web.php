<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongControllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});


//songlist
Route::post('/addsong', [SongControllers::class, 'insertSong']);
Route::get('/displaying', [SongControllers::class, 'displaySong']);
Route::post('/deletesong', [SongControllers::class, 'deletesong']);

//playlist
Route::post('/addplaylist', [SongControllers::class, 'insertPlaylist']);
Route::get('/displayplaylist', [SongControllers::class, 'displayPlaylist']);
Route::post('/deleteplaylist', [SongControllers::class, 'deleteplaylist']);
Route::post('/editplaylist', [SongControllers::class, 'editplaylist']);

//addsong to playlist
Route::post('/addsongtoplaylist', [SongControllers::class, 'addsongtoplaylist']);
Route::get('/displaysongtoplaylist/{id}', [SongControllers::class, 'displaysongtoplaylist']);
Route::post('/deletesongfromplaylist', [SongControllers::class, 'deletesongfromplaylist']);
Route::get('/displaysongnotinplaylist/{id}', [SongControllers::class, 'displaysongnotinplaylist']);

 Route::post('/test', [SongControllers::class, 'test']);




